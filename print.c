/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ftriquet <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/07 18:39:20 by ftriquet          #+#    #+#             */
/*   Updated: 2015/12/10 14:35:12 by ftriquet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"
#include "libft.h"

int			put_square(t_square *s)
{
	size_t	i;
	size_t	j;

	i = 0;
	while (i < s->min_size)
	{
		j = 0;
		while (j < s->min_size)
		{
			ft_putchar(s->matrix[i][j]);
			j++;
		}
		ft_putchar('\n');
		i++;
	}
	return (1);
}

void		print_tetris(t_tetri *t)
{
	int	i;
	int	j;

	i = 0;
	while (i < 4)
	{
		j = 0;
		while (j < 4)
		{
			ft_putchar(t->shape[i][j]);
			j++;
		}
		ft_putchar('\n');
		i++;
	}
	ft_putchar('\n');
	if (t->next)
		print_tetris(t->next);
}
