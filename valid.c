/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   valid.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ftriquet <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/07 18:29:44 by ftriquet          #+#    #+#             */
/*   Updated: 2015/12/10 14:23:07 by ftriquet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "fillit.h"

int			check_arround(char tab[4][5], int i, int j, char letter)
{
	int		it;
	int		jt;
	int		voisins;

	voisins = 0;
	it = 0;
	while (it < 4)
	{
		jt = 0;
		while (jt < 4)
		{
			if (tab[it][jt] == letter)
				if (it != i || jt != j)
				{
					if (it == i && (jt == j - 1 || jt == j + 1))
						voisins++;
					if (jt == j && (it == i - 1 || it == i + 1))
						voisins++;
				}
			jt++;
		}
		it++;
	}
	return (voisins);
}

int			is_valid(t_tetri *t)
{
	int		count;
	int		i;
	int		j;
	int		total;

	count = 0;
	total = 0;
	i = -1;
	if (!t)
		return (0);
	while (++i < 4)
	{
		j = -1;
		while (++j < 4)
		{
			if (t->shape[i][j] == t->letter)
			{
				total += check_arround(t->shape, i, j, t->letter);
				count++;
			}
			else if (t->shape[i][j] != '.')
				return (0);
		}
	}
	return (count == 4 && (total == 6 || total == 8));
}

int			check_line(char a[6])
{
	int		i;

	i = 0;
	while (i < 4)
	{
		if (a[i] != '.' && a[i] != '#')
			return (0);
		i++;
	}
	return (a[i] == '\n');
}
