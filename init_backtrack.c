/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_backtrack.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ftriquet <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/07 18:37:01 by ftriquet          #+#    #+#             */
/*   Updated: 2015/12/08 14:32:24 by ftriquet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"
#include "libft.h"

size_t	min_size(size_t nb)
{
	size_t	i;

	i = 1;
	while (i * i < nb * 4)
		i++;
	return (i);
}

void	init_square(t_square *s)
{
	int	i;

	i = 0;
	while (i < 26)
	{
		ft_memset(s->matrix[i], '.', 26);
		i++;
	}
}
