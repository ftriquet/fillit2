# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: ftriquet <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/12/08 00:11:51 by ftriquet          #+#    #+#              #
#    Updated: 2015/12/10 13:42:17 by ftriquet         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

SRC = backtrack.c init_backtrack.c main.c pack.c print.c read_tetris.c valid.c

OBJ = $(SRC:.c=.o)

NAME = fillit

LIB_PATH = ./libft/

LIB_NAME = libft.a

LIB = $(addprefix $(LIB_PATH), $(LIB_NAME))

CFLAGS = -Wall -Wextra -Werror

all: $(NAME)

$(NAME): $(LIB) $(OBJ)
	gcc $(CFLAGS) $(OBJ) -lft -L $(LIB_PATH) -o $(NAME)

%.o: %.c
	gcc $(CFLAGS) -c $< -o $@ -I $(LIB_PATH)

$(LIB):
	make -C $(LIB_PATH)

clean:
	make clean -C $(LIB_PATH)
	rm -f $(OBJ)

fclean: clean
	make fclean -C $(LIB_PATH)
	rm -f $(NAME)

re: fclean $(NAME)
