/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pack.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ftriquet <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/07 18:25:16 by ftriquet          #+#    #+#             */
/*   Updated: 2015/12/08 14:33:34 by ftriquet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"
#include "libft.h"

void		pack_tetri(t_tetri *t)
{
	int		i;

	if (!t)
		return ;
	while (!ft_strchr(t->shape[0], t->letter))
	{
		i = 0;
		while (i < 3)
		{
			ft_strncpy(t->shape[i], t->shape[i + 1], 4);
			ft_strncpy(t->shape[i + 1], "....", 4);
			i++;
		}
	}
	while (!check_first_col(t->shape, t->letter))
		pack_cols(t->shape);
	t->width = 0;
	t->height = 0;
}

void		pack_cols(char tab[4][5])
{
	int		i;
	int		j;

	i = 0;
	while (i < 4)
	{
		j = 0;
		while (j < 3)
		{
			tab[i][j] = tab[i][j + 1];
			tab[i][j + 1] = '.';
			j++;
		}
		i++;
	}
}

int			check_first_col(char tab[4][5], char letter)
{
	int		i;

	i = 0;
	while (i < 4)
	{
		if (tab[i][0] == letter)
			return (1);
		i++;
	}
	return (0);
}
