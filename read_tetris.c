/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_tetris.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ftriquet <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/07 18:19:02 by ftriquet          #+#    #+#             */
/*   Updated: 2015/12/10 14:49:14 by ftriquet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"
#include "libft.h"
#include <stdlib.h>
#include <fcntl.h>

t_tetri		*read_tetri(int fd, char letter)
{
	char	a[6];
	int		i;
	t_tetri	*tetri;

	tetri = malloc(sizeof(t_tetri));
	if (!tetri)
		return (NULL);
	i = 0;
	while (i < 4)
	{
		ft_bzero(a, 6);
		read(fd, a, 5);
		if (!check_line(a))
		{
			free(tetri);
			return (NULL);
		}
		ft_strncpy((tetri->shape)[i], a, 4);
		(tetri->shape)[i][4] = '\0';
		ft_strreplace(tetri->shape[i], '#', letter);
		i++;
	}
	tetri->letter = letter;
	tetri->next = NULL;
	return (tetri);
}

t_tetri		*read_tetris(int fd, int *nb_tetris)
{
	t_tetri	*res;
	t_tetri	*it;
	char	c;

	it = read_tetri(fd, 'A' + *nb_tetris);
	pack_tetri(it);
	res = it;
	(*nb_tetris)++;
	if (!is_valid(res))
		del_tetris(&res, nb_tetris);
	init_size(res);
	if (res)
		while (res && read(fd, &c, 1) != 0)
		{
			it->next = read_tetri(fd, 'A' + *nb_tetris);
			it = it->next;
			(*nb_tetris)++;
			if (!it || !is_valid(it) || c != '\n')
				del_tetris(&res, nb_tetris);
			pack_tetri(it);
			init_size(it);
		}
	return (res);
}

void		init_size(t_tetri *t)
{
	size_t	h;
	size_t	w;
	size_t	j;
	size_t	i;

	i = -1;
	w = 0;
	h = 0;
	if (t)
	{
		while (++i < 4)
		{
			j = -1;
			while (++j < 4)
			{
				if (t->shape[i][j] == t->letter)
					w = j + 1 > w ? j + 1 : w;
				if (t->shape[j][i] == t->letter)
					h = j + 1 > h ? j + 1 : h;
			}
		}
		t->height = h > t->height ? h : t->height;
		t->width = w > t->width ? w : t->width;
	}
}

void		del_tetris(t_tetri **t, int *nb_tetris)
{
	if (nb_tetris)
		*nb_tetris = 0;
	if (t && *t)
	{
		if ((*t)->next)
			del_tetris(&(*t)->next, nb_tetris);
		free(*t);
		*t = NULL;
	}
}
