/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   backtrack.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ftriquet <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/07 18:41:52 by ftriquet          #+#    #+#             */
/*   Updated: 2015/12/08 14:39:07 by ftriquet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"
#include "libft.h"

void	yoloswag2(t_square *s, t_tetri *list)
{
	while (1)
	{
		if (process2(s, list))
			return ;
		else
			s->min_size++;
	}
}

int		process2(t_square *s, t_tetri *list)
{
	size_t	i;
	size_t	j;

	i = 0;
	while (i < s->min_size - list->height + 1)
	{
		j = 0;
		while (j < s->min_size - list->width + 1)
		{
			if (putable(s, list, i, j))
			{
				put(s, list, i, j);
				if (list->next == NULL)
					return (put_square(s));
				else if (process2(s, list->next))
					return (1);
				remove_tetri(s, list);
			}
			j++;
		}
		i++;
	}
	return (0);
}

int		putable(t_square *s, t_tetri *t, size_t i, size_t j)
{
	size_t	it;
	size_t	jt;

	it = 0;
	if (i + t->height > s->min_size || j + t->width > s->min_size)
		return (0);
	while (it < t->height)
	{
		jt = 0;
		while (jt < t->width)
		{
			if (s->matrix[i + it][j + jt] != '.' && t->shape[it][jt] != '.')
				return (0);
			jt++;
		}
		it++;
	}
	return (1);
}

void	put(t_square *s, t_tetri *t, size_t i, size_t j)
{
	size_t	it;
	size_t	jt;

	it = 0;
	while (it < t->height)
	{
		jt = 0;
		while (jt < t->width)
		{
			if (t->shape[it][jt] == t->letter)
				s->matrix[i + it][j + jt] = t->letter;
			jt++;
		}
		it++;
	}
}

void	remove_tetri(t_square *s, t_tetri *t)
{
	size_t	i;
	size_t	j;

	i = 0;
	while (i < s->min_size)
	{
		j = 0;
		while (j < s->min_size)
		{
			if (s->matrix[i][j] == t->letter)
				s->matrix[i][j] = '.';
			j++;
		}
		i++;
	}
}
