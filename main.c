/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ftriquet <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/07 23:55:20 by ftriquet          #+#    #+#             */
/*   Updated: 2015/12/10 13:11:02 by ftriquet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

void	start_backtrack(t_square *s, int *nb_tetris, t_tetri *list)
{
	init_square(s);
	s->min_size = min_size(*nb_tetris);
	yoloswag2(s, list);
	del_tetris(&list, nb_tetris);
}

int		main(int argc, char **argv)
{
	int			fd;
	int			nb_tetris;
	t_tetri		*list;
	t_square	s;

	nb_tetris = 0;
	if (argc == 2 && (fd = open(argv[1], O_RDONLY)) != -1)
	{
		list = read_tetris(fd, &nb_tetris);
		if (list == NULL || nb_tetris > 26)
		{
			if (list != NULL)
				free(list);
			ft_putendl("error");
			return (0);
		}
		start_backtrack(&s, &nb_tetris, list);
	}
	else
		ft_putendl("error");
	return (0);
}
