/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ftriquet <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/08 14:38:14 by ftriquet          #+#    #+#             */
/*   Updated: 2015/12/10 13:41:38 by ftriquet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILLIT_H
# define FILLIT_H

# include "libft.h"
# include <fcntl.h>
# include <unistd.h>
# include <stdlib.h>

typedef struct		s_tetri
{
	char			letter;
	struct s_tetri	*next;
	char			shape[4][5];
	size_t			width;
	size_t			height;
}					t_tetri;

typedef struct		s_square
{
	size_t			min_size;
	char			matrix[26][26];

}					t_square;

t_tetri				*read_tetri		(int fd, char letter);
void				print_tetris	(t_tetri *t);
int					check_first_col	(char tab[4][5], char letter);
void				pack_cols		(char tab[4][5]);
void				pack_tetri		(t_tetri *t);
int					check_arround	(char tab[4][5], int i, int j, char letter);
int					is_valid		(t_tetri *t);
int					check_line		(char a[6]);
void				del_tetris		(t_tetri **t, int *nb_tetris);
void				init_size		(t_tetri *t);
t_tetri				*read_tetris	(int fd, int *nb_tetris);
int					putable			(t_square *s, t_tetri *t, size_t i,
		size_t j);
void				put				(t_square *s, t_tetri *t, size_t i,
		size_t j);
void				init_square		(t_square *s);
int					put_square		(t_square *s);
void				remove_tetri	(t_square *s, t_tetri *t);
size_t				min_size		(size_t nb);
void				free_tetris_tab	(t_tetri **tab, size_t size);
void				yoloswag2		(t_square *s, t_tetri *list);
int					process2		(t_square *s, t_tetri *list);
void				start_backtrack	(t_square *s, int *nb_tetris,
		t_tetri *list);
#endif
